# README #
### What is this repository for? ###

* Zulu Hotel Australia 2016
* Currently being converted to version POL0.99

### How do I get set up? ###

* Scripts should just need to be downloaded (suggest sourcetree).
* Change the two executables from .exe.RENAME to just .exe (/pol.exe and /scripts/ecompile.exe)
* Open the ecompile.cfg folder in /scripts/ and relocate to match your OS folder structure.
* Modify /pol.cfg so that it points to your UO directory.
* Modify /config/servers.cfg so that the ip's match accordingly.
* Open a CMD prompt and navigate to the scripts folder and run ecompile.bat
* Once ecompile finishes go back up a level and run pol.exe

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Jonathan Morland-Barrett (Dev GIB) - jmorland.barrett@gmail.com
* Craig Bruce (Harlz) - craigbruce32@gmail.com