function getskilllist(me)
  var mytemplate := GetObjProperty(me, "MerchantType");
  case(mytemplate)
    "Healer":           return array{ "spiritspeaking", "healing", "anatomy", "camping", "fishing", "forensic evaluation", "piety" };
    "stable":           return array{ "animal lore", "animal taming", "veterinary" };
    "ShipWright":       return array{ "wrestling", "tactics" };
    "fisherman":        return array{ "fishing", "fencing" };
    "innkeeper":        return array{ "fencing", "tactics" };
    "farmer":           return array{ "swordsmanship", "animal lore", "herding", "veterinary", "naturelore" };
    "Weaponsmith":      return array{ "swordsmanship", "arms lore", "blacksmithy", "mining", "parrying", "tactics" };
    "Armorer":          return array{ "mace fighting", "arms lore", "blacksmithy", "mining", "parrying", "tactics" };
    "barkeep":          return array{ "mace fighting", "tactics" };
    "barmaid":          return array{ "fencing", "tactics" };
    "baker":            return array{ "wrestling", "cooking" };
    "tinker":           return array{ "fencing", "alchemy", "lockpicking", "tinkering" };
    "bard":             return array{ "fencing", "enticement", "musicianship", "provocation", "peacemaking" };
    "butcher":          return array{ "mace fighting", "cooking", "taste identification" };
    "cobbler":          return array{ "swordsmanship", "tailoring" };
    "jeweler":          return array{ "fencing", "item identification" };
    "Mage":             return array{ "magic resistance", "inscription", "magery", "meditation", "wrestling", "necromancy" };
    "Alchemist":        return array{ "magic resistance", "taste identification", "alchemy", "wrestling", "necromancy" };
    "Herbalist":        return array{ "magic resistance", "alchemy", "cooking", "taste identification", "wrestling" };
    "Tailor":           return array{ "tailoring", "fencing" };
    "Weaver":           return array{ "tailoring", "fencing" };
    "Leatherworker":    return array{ "parrying", "mace fighting" };
    "Provisioner":      return array{ "tactics", "parrying", "fencing" };
    "Carpenter":        return array{ "carpentry", "lumberjacking", "swordsmanship" };
    "Bowyer":           return array{ "archery", "bowcraft", "fencing" };
    "Architect":        return array{ "magic resistance", "wrestling" };
    "Scribe":           return array{ "magic resistance", "evaluating intelligence", "wrestling" };
    "Mapmaker":         return array{ "cartography", "wrestling" };
    "Vetrinarian":      return array{ "animal lore", "wrestling", "veterinary" };
    "TownGuard":        return array{ "anatomy", "tactics", "swordsmanship", "detect hidden", "arms lore", "psionics" };
    "Thief":            return array{ "snooping", "stealing", "remove trap", "stealth", "fencing", "poisoning", "lockpicking", "hiding" };
    "Beggar":           return array{ "begging", "wrestling", "cooking", "fishing" };
    "Fighter1":         return array{ "anatomy", "tactics", "swordsmanship", "arms lore" };
    "Fighter2":         return array{ "anatomy", "tactics", "swordsmanship", "arms lore" };
    "Fighter3":         return array{ "anatomy", "tactics", "swordsmanship", "arms lore" };
    "Fighter4":         return array{ "anatomy", "tactics", "swordsmanship", "arms lore" };
    "Paladin":          return array{ "anatomy", "tactics", "swordsmanship", "detect hidden", "arms lore" };
    "Artist":
  endcase
endfunction

function FindSkillId(name)
  case(name)
    "alchemy":                  return  0;
    "anatomy":                  return  1;
    "animal lore":              return  2;
    "item identification":      return  3;
    "arms lore":                return  4;
    "parrying":                 return  5;
    "begging":                  return  6;
    "blacksmithy":              return  7;
    "bowcraft":                 return  8;
    "peacemaking":              return  9;
    "camping":                  return 10;
    "carpentry":                return 11;
    "cartography":              return 12;
    "cooking":                  return 13;
    "detect hidden":            return 14;
    "enticement":               return 15;
    "evaluating intelligence":  return 16;
    "healing":                  return 17;
    "fishing":                  return 18;
    "forensic evaluation":      return 19;
    "herding":                  return 20;
    "hiding":                   return 21;
    "provocation":              return 22;
    "inscription":              return 23;
    "lockpicking":              return 24;
    "magery":                   return 25;
    "magic resistance":         return 26;
    "tactics":                  return 27;
    "snooping":                 return 28;
    "musicianship":             return 29;
    "poisoning":                return 30;
    "archery":                  return 31;
    "spiritspeaking":           return 32;
    "stealing":                 return 33;
    "tailoring":                return 34;
    "animal taming":            return 35;
    "taste identification":     return 36;
    "tinkering":                return 37;
    "tracking":                 return 38;
    "veterinary":               return 39;
    "swordsmanship":            return 40;
    "mace fighting":            return 41;
    "fencing":                  return 42;
    "wrestling":                return 43;
    "lumberjacking":            return 44;
    "mining":                   return 45;
    "meditation":               return 46;
    "stealth":                  return 47;
    "remove trap":              return 48;
    "nature lore":	        return 49;
    "piety":			return 50;
    "necromancy":		return 51;
    "psionics":			return 52;
  endcase
endfunction
 