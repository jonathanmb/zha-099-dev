function main_AI_loop()
	var ev;
   EnableMainEvents();
	me.hidden:=1;
	var mydestx := 0;
	var mydesty := 0;
	var steps := 0;
	
	SetAnchor( CInt(me.x), CInt(me.y), 4, 50 );
	
   while (1)
		ev := os::wait_for_event( 2 );
		if (ev)
        repeat
           case (ev.type)
 				SYSEVENT_ENTEREDAREA:
					me.hidden:=1;
					ReturnHome();
					ReportToMaster(ev.source);
	    
	endcase

        until (! (ev := os::wait_for_event(2)) );
		endif

   endwhile        

endfunction       

function EnableMainEvents()

    DisableEvents( SYSEVENT_LEFTAREA + SYSEVENT_DISENGAGED + SYSEVENT_OPPONENT_MOVED );
    EnableEvents( SYSEVENT_SPEECH + SYSEVENT_ENGAGED + SYSEVENT_DAMAGED + SYSEVENT_ENTEREDAREA, HALT_THRESHOLD );
    EnableEvents( SYSEVENT_SPEECH, 3 );
    DisableEvents(SYSEVENT_ITEM_GIVEN);

endfunction

function DisableMainEvents()

    DisableEvents( SYSEVENT_SPEECH + SYSEVENT_ENGAGED + SYSEVENT_DAMAGED );
    DisableEvents(SYSEVENT_ITEM_GIVEN);

endfunction

function ReturnHome()
	var myhome := GetObjProperty(me,"myhome");
	MoveObjectToLocation(me,myhome[1],myhome[2],myhome[3],"britannia",MOVEOBJECT_FORCELOCATION);
endfunction

function ReportToMaster(mobile_entered)
	SendSysMessage(me.master, me.name +": " + mobile_entered.name + " is near me!",9,73);

endfunction